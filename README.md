# git-training

This is a sandbox repo for a training session

## usage

The project consists of a minimalist python CLI hello-world app
```shell
$ python3 -m greet --name world
Hello world !
```

## testing

Requirements:
Python3, pytest

```shell
# get pytest
$ python3 -m pip install pytest
```

run it from the top level of the repo
```
$ pytest
```
## contribution guidelines

- fork this repo
- create a branch related to an existing issue
- ... work on your patch ...
- submit a merge-request
